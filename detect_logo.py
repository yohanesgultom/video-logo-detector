#!/usr/bin/python3
'''
Detect logo in video file or RTMP

Examples:

python3 detect_logo.py rtmp://188.166.238.78:1938/sportfixasia/stream_01_sg mask_1080.png mask_result_1080.png 1436,98 --cmd1 "ffmpeg -re -i rtmp://188.166.238.78:1938/sportfixasia/stream_01_sg -c:v libx264 -preset veryfast  -c:a aac  -f flv rtmp://tvonestream:birumuda@edgestream-sg.vivagroup.co.id:1936/tvonenoads/tvone" --cmd0 "ffmpeg -re -i video.mp4 -c:v libx264 -preset veryfast -c:a aac -b:v 5000k -f flv rtmp://tvonestream:birumuda@edgestream-sg.vivagroup.co.id:1936/tvonenoads/tvone"

python3 detect_logo.py -c -p 10 rtmp://188.166.238.78:1938/sportfixasia/stream_01_sg mask_1080.png mask_result_1080.png 1436,98

python3 detect_logo.py -c -p 15 ~/Downloads/tvone_live_switch.mp4 mask_720.png mask_result_720.png 960,50

'''

import matplotlib.pyplot as plt
import numpy as np
import subprocess
import threading
import argparse
import signal
import shlex
import time
import cv2
from datetime import datetime


class Terminator:
  terminate_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self,signum, frame):
    self.terminate_now = True

def to_bw(img):
    '''
    Convert RGB image array to monochrome where each cell x = {0, 255}
    '''
    threshold = np.max(img) / 2
    return np.where(img > threshold, 255, 0).astype(np.uint8)

def detect_color_image(img, sample):
    '''
    Return average RGB value difference between img and sample x, where 0.0 <= x <= 1.0
    Smaller value means img is more similar to sample. Consequently, bigger value means img is less similar to sample
    '''
    if img.shape[0] != sample.shape[0] or img.shape[1] != sample.shape[1]:
        raise Exception('images size {} must equal {}'.format(img.shape, sample.shape))

    h = img.shape[0]
    w = img.shape[1]

    diff = 0
    for y in range(0, h):
        for x in range(0, w):
            # threshold the pixel
            b, g, r = img[y, x]
            b1, g1, r1 = sample[y, x]
            diff += abs(int(r) - int(r1)) + abs(int(g) - int(g1)) + abs(int(b) - int(b1))
    
    return diff / (w * h)  / (3.0 * 255)

def delayed_terminate(p, d):
    '''
    Terminate process p after d seconds
    '''
    if p is not None:
        time.sleep(d)
        p.terminate()
        p.wait()

def save_score_chart(frames, scores, labels):
    plt.plot(frames, scores, color='blue') 
    plt.plot(frames, labels, color='red')
    plt.legend(['score', 'label'], loc='upper left')
    plt.suptitle('Color scores')                                                                                                            
    plt.title('Updated at {}'.format(datetime.now()), fontdict={'fontsize': 9})
    plt.xlabel('frame')
    plt.grid(True)
    plt.draw()
    plt.savefig('color_scores.png')

def start(video_path,
    mask_path,
    positive_path,
    logo_top_left,
    cmd1,
    cmd0,
    threshold=0.1,
    consistency=2,
    cmd_overlap=0,
    output='out.log',
    frames=False,
    chart=False,
    chart_period=100,
    test_boundary=False):
    '''
    Run logo detector forever to switch between cmd1 when logo detected and cmd0 when not
    '''
    frames_dir = 'frames'

    # get mask and make sure it is black and white
    mask = cv2.imread(mask_path, 0)
    mask = to_bw(mask)
    sample = cv2.imread(positive_path)

    # capture video
    video = cv2.VideoCapture(video_path)
    
    # print spec
    fps = video.get(cv2.CAP_PROP_FPS)
    w = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    total_frame = video.get(cv2.CAP_PROP_FRAME_COUNT)
    print('Total_frame: {}'.format(total_frame))
    print('Spec: {}x{}p {} fps'.format(w, h, fps))

    # derive logo boundary from top left coordinate and mask size
    tl = logo_top_left
    r = ((tl[0], tl[1]), (tl[0]+mask.shape[1], tl[1]+mask.shape[0]))
    
    scores = [] 
    labels = [] 
    frames = []   

    # output file
    fout = open(output, 'w+')

    # number of frame to skip
    # frame_skip = min(fps, 60)
    frame_skip = fps
    frame_pos = 0
    consistency = 0
    prev_label_actual = None
    prev_label = None
    p = None

    terminator = Terminator()
    while True and (total_frame < 0 or frame_pos < total_frame):
        # start reading frame
        flag, frame = video.read()
        # The frame is ready and already captured
        # but read only first frame per second
        # frame_pos = int(video.get(cv2.CAP_PROP_POS_FRAMES))
        if flag and frame_pos % frame_skip == 0:
            start = time.time()        
            # test boundary
            if test_boundary:
                cv2.rectangle(frame, *r, (0,255,0), 2)
                cv2.imwrite('{}/{}_test.png'.format(frames_dir, frame_pos), frame)

            logo = frame[r[0][1]:r[1][1], r[0][0]:r[1][0]]   
            # apply mask
            res = cv2.bitwise_and(logo, logo, mask=mask)		 
            score = detect_color_image(res, sample)
            
            # label = 1 if positive[0] <= score <= positive[1] else 0
            label = 1 if score <= threshold else 0
            
            # track consistency
            if prev_label_actual is not None:                
                if label == prev_label_actual:
                    consistency = consistency + 1
                else:
                    consistency = 0
                tmp = label

                # prevent label change if lack of consistency
                if label != prev_label_actual and consistency < consistency:
                    label = prev_label_actual

                prev_label_actual = tmp
            
            scores.append(score)		  
            frames.append(frame_pos)
            labels.append(label)

            # run command
           
            # Method 1: kill the previous command, then spawn the new command
            # if prev_label is None or prev_label != label:                
            #     threading.Thread(target=delayed_terminate, (p, 0)).start()
            #     cmd = cmd1 if label == 1 else cmd0
            #     p = subprocess.Popen(cmd.split(), stdout=fout) if cmd is not None else None
            #     prev_label = label

            # Method 2: spawn the new command, kill previous command after x second
            if prev_label is None or prev_label != label:
                cmd = cmd1 if label == 1 else cmd0
                prev_p = p # save previous process
                p = subprocess.Popen(shlex.split(cmd), stdout=fout) if cmd is not None else None
                prev_label = label
                threading.Thread(target=delayed_terminate, args=(prev_p, cmd_overlap)).start()

            # restart current process if dead (because of error)
            if p is not None and p.poll() is not None:
                cmd = cmd1 if label == 1 else cmd0
                p = subprocess.Popen(shlex.split(cmd), stdout=fout) if cmd is not None else None

            # save frame to frames_dir
            if frames:
                cv2.imwrite('{}/{}_masked.png'.format(frames_dir, frame_pos), res)
                # cv2.imwrite('{}/{}.png'.format(frames_dir, frame_pos), logo)

            # plot scores
            if chart and len(frames) % chart_period == 0:
                save_score_chart(frames, scores, labels)

            # print('Time: {:.4f} s'.format(time.time()-start))
        if flag:
            frame_pos = frame_pos + 1

        if terminator.terminate_now:
            if p:
                p.terminate()
                p.wait()
            break

    fout.close()    

def tuple_int(s):
    '''
    Parse string into tuple of integer
    '''
    try:
        x, y = map(int, s.split(','))
        return (x, y)
    except:
        raise argparse.ArgumentTypeError("Tuple int must be: x,y")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Detect logo in video')
    parser.add_argument('video_path', help='Path to the video file or RTMP URL')
    parser.add_argument('mask_path', help='Path to the monochrome logo mask (logo is white, background is black)')
    parser.add_argument('positive_path', help='Path to the positive logo sample')
    parser.add_argument('logo_top_left', type=tuple_int, help="x,y Logo top left boundary coordinate")
    parser.add_argument('--cmd1', help="Command to run when label is 1")
    parser.add_argument('--cmd0', help="Command to run when label is 0")
    parser.add_argument('--threshold', type=float, default=0.1, help="Max threshold for positive label score, where 0.0 <= threshold <= 1.0")
    parser.add_argument('--consistency', type=int, default=2, help="Minimum sequential occurrence of the label before considered as valid")
    parser.add_argument('--cmd_overlap', type=int, default=0, help="Overlapping time in second when switching between command")
    parser.add_argument('-o', '--output', default='out.log', help='Command log output file')
    parser.add_argument('-f', '--frames', action='store_true', default=False, help='Save frame images')
    parser.add_argument('-c', '--chart', action='store_true', default=False, help='Generate score per frame chart')
    parser.add_argument('-p', '--chart_period', type=int, default=100, help="Chart update period")
    parser.add_argument('-t', '--test_boundary', action='store_true', default=False, help='Draw boundary to frames and save them')
    args = parser.parse_args()
    start(
        args.video_path,
        args.mask_path,
        args.positive_path,
        args.logo_top_left,
        args.cmd1,
        args.cmd0,
        args.threshold,
        args.consistency,
        args.cmd_overlap,
        args.output,
        args.frames,
        args.chart,
        args.chart_period,
        args.test_boundary
    )



    