#!/usr/bin/python3

'''
Manage logo detection processes based according to database records

Examples:

python3 process_manager.py -d rtvp -u root -p root -o "out.log" -s "/home/yohanesgultom/Workspace/realtime-video-replacer-web/storage/app/"
'''

import subprocess
import threading
import argparse
import datetime
import logging
import pymysql
import json
import time
from os import path
from detect_logo import Terminator, delayed_terminate

MYSQL_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
SELECT_STREAM_TARGET_SQL = '''
select 
    s.id, 
    t.rtmp_source, 
    s.rtmp_target, 
    s.start, 
    s.end, 
    s.ffmpeg_args_0, 
    s.ffmpeg_args_1, 
    t.logo_mask, 
    t.logo_masked, 
    t.logo_x, 
    t.logo_y, 
    s.options, 
    a.file as ad_file 
from stream_targets s 
    join ads a on s.ad_id = a.id 
    join programs p on s.program_id = p.id
    join tvs t on p.tv_id = t.id
where s.start <= %s and s.end > %s
    and t.rtmp_source is not null
    and s.rtmp_target is not null
    and s.ffmpeg_args_0 is not null
    and s.ffmpeg_args_1 is not null
    and t.logo_mask is not null
    and t.logo_masked is not null
    and t.logo_x is not null
    and t.logo_y is not null
    and a.file is not null
'''

def create_process_key(values, sep='*'):
    '''
    Create key by concatenating values as string
    '''
    return sep.join([str(x) for x in values])

def main(database, user, password, host, port, file_storage, out):
    '''
    Main process
    '''
    terminator = Terminator()
    processes = {}    
    while True:
        try:
            t1 = time.time()
            db = pymysql.connect(host, user, password, database, port)
            cursor = db.cursor()
            now = datetime.datetime.now()
            cursor.execute(SELECT_STREAM_TARGET_SQL, (now, now))
            data = cursor.fetchall()
            active_keys = []
            for row in data:
                t_id, rtmp_source, rtmp_target, start, end, ffmpeg_args_0, ffmpeg_args_1, logo_mask, logo_masked, logo_x, logo_y, options, ad_file = row
                p_key = create_process_key([t_id, rtmp_source, rtmp_target, ffmpeg_args_0, ffmpeg_args_1, logo_mask, logo_masked, logo_x, logo_y, options, ad_file])

                # run process if not yet running or updated or dead
                if p_key not in processes or processes[p_key].poll() is not None:
                    options = json.loads(options) if options else None
                    video_path = path.join(file_storage, ad_file)
                    mask_path = path.join(file_storage, logo_mask)
                    positive_path = path.join(file_storage, logo_masked)
                    # logo_top_left = (logo_x, logo_y)
                    cmd1 = 'ffmpeg ' + ffmpeg_args_1.format(rtmp_source=rtmp_source, rtmp_target=rtmp_target)
                    cmd0 = 'ffmpeg ' + ffmpeg_args_0.format(ad_file=video_path, rtmp_target=rtmp_target)
                    threshold = options['threshold'] if 'threshold' in options else 0.1
                    consistency = options['consistency'] if 'consistency' in options else 2
                    cmd_overlap = options['cmd_overlap'] if 'cmd_overlap' in options else 0
                    output = open('logs/{}.log'.format(t_id), 'w+')

                    processes[p_key] = subprocess.Popen([
                        'python3', 
                        'detect_logo.py', 
                        rtmp_source, 
                        mask_path, 
                        positive_path, 
                        '{},{}'.format(logo_x, logo_y),
                        '--cmd1', cmd1, 
                        '--cmd0', cmd0,
                        '--threshold', str(threshold),
                        '--consistency', str(consistency),
                        '--cmd_overlap', str(cmd_overlap),
                    ], stdout=output, stderr=output)

                active_keys.append(p_key)

            # kill other processes
            for p_key in list(processes):
                if p_key not in active_keys:
                    threading.Thread(target=delayed_terminate, args=(processes[p_key], 0)).start()
                    del processes[p_key]

            if out:
                with open(out, 'w+') as f:
                    f.write(json.dumps({
                        'active_keys': active_keys,
                        'processes': list(processes),
                        'exec_time': round(time.time() - t1, 4),
                        'updated_at': datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
                    }, indent=4))

            db.close()
            time.sleep(0.8)
        except Exception as e:
            logging.exception(e)
            db.close()

        # terminate all subprocess on sigterm
        if terminator.terminate_now:
            for tid in list(processes):
                p = processes[tid]
                p.terminate()
                p.wait()
            break            

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Manage logo detection processes based according to database records')
    required_named = parser.add_argument_group('required named arguments')
    required_named.add_argument('-d', '--db', required=True, help='Database name')
    required_named.add_argument('-u', '--user', required=True, help='Database username')
    required_named.add_argument('-p', '--password', required=True, help='Database password')
    required_named.add_argument('-s', '--storage', required=True, help='File storage absolute path')
    parser.add_argument('--host', default='localhost', help='Database host server')
    parser.add_argument('--port', default=3306, help='Database port')
    parser.add_argument('-o', '--out', help='File output of latest status summary')
    args = parser.parse_args()
    main(args.db, args.user, args.password, args.host, args.port, args.storage, args.out)
