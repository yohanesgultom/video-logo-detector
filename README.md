# video-logo-detector

Exploration on binary logo detection (logo/not logo) on real time video

## Setup

Prerequisites:

* Python >= 3.x https://www.python.org/downloads/
* OpenCV >= 4.x https://docs.opencv.org/trunk/df/d65/tutorial_table_of_content_introduction.html

Configuration:

1. Clone this repository
1. Run command from inside repository directory to install from dependencies: `pip3 install -r requirements.txt`

Running:

1. Start logo detection by running: `python3 detect_logo.py {video_path} {logo_mask_path} {logo_positive_sample_path} {logo_top_left_coordinate}`
1. To trigger command based on detection result, use option `--cmd0 "{command}"` and `--cmd1 "{command}"` for negative and positive result respectively
1. Generate detection chart using option `-c`. By default chart will be updated every 100 seconds. To change this use `-p {time_in_seconds}`
1. More options and details can be seen using option `-h`

Running command examples:

* Detect logo in video file `python3 detect_logo.py -c -p 15 ~/Downloads/tvone_live_switch.mp4 mask_720.png mask_result_720.png 960,50`

* Detect logo in livestream (RTMP) file `python3 detect_logo.py -c -p 10 rtmp://188.166.238.78:1938/sportfixasia/stream_01_sg mask_1080.png mask_result_1080.png 1436,98`

* Detect logo in livestream (RTMP) file and trigger FFMPEG command based on result `python3 detect_logo.py rtmp://188.166.238.78:1938/sportfixasia/stream_01_sg mask_1080.png mask_result_1080.png 1436,98 --cmd1 "ffmpeg -re -i rtmp://188.166.238.78:1938/sportfixasia/stream_01_sg -c:v libx264 -preset veryfast  -c:a aac  -f flv rtmp://tvonestream:birumuda@edgestream-sg.vivagroup.co.id:1936/tvonenoads/tvone" --cmd0 "ffmpeg -re -i video.mp4 -c:v libx264 -preset veryfast -c:a aac -b:v 5000k -f flv rtmp://tvonestream:birumuda@edgestream-sg.vivagroup.co.id:1936/tvonenoads/tvone"`


Chart example:

![Chart example](chart_example_1.png)